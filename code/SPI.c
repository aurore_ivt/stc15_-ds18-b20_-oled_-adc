#include "STC15W408AS.h"
#include "SPI.h"

void spi_init(void)
{
	SPDAT=0;
	SPSTAT=0XC0;//清除传输完成和写入冲突标志
	SPCTL=0XD0; //SSIG=1表示由MSTR位确定单片机是主机还是从机
				//MSTR=1,单片机是主机
				//DORD=0,高位在前
				//空闲时位低电平，上升沿采样
				//SPI时钟plv=CPU/4
				//SPE=1，使能SPI
}
unsigned char spi_rw(unsigned char dat)
{
	SPDAT=dat;
	while(!(SPSTAT&0X80));
	SPSTAT=0XC0;
	
	return SPDAT;
}

