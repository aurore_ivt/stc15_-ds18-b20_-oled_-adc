/*---------------------------------------------------------------------*/
/* --- STC MCU International Limited ----------------------------------*/
/* --- STC 1T Series MCU Demo Programme -------------------------------*/
/* --- Mobile: (86)13922805190 ----------------------------------------*/
/* --- Fax: 86-0513-55012956,55012947,55012969 ------------------------*/
/* --- Tel: 86-0513-55012928,55012929,55012966 ------------------------*/
/* --- Web: www.GXWMCU.com --------------------------------------------*/
/* --- QQ:  800003751 -------------------------------------------------*/
/* 如果要在程序中使用此代码,请在程序中注明使用了宏晶科技的资料及程序   */
/*---------------------------------------------------------------------*/

/*************	功能说明	**************

本文件为STC15xxx系列的定时器初始化和中断程序,用户可以在这个文件中修改自己需要的中断程序.

定时器0做16位自动重装10us定时，不中断，从P3.5高速输出脉宽10us 50KHZ时钟信号。
    用户可以修改时间从而修改频率，也可以允许中断，但注意时间不要太短（一般10us以上）。

定时器1做16位自动重装1ms定时， 中断，从P3.4高速输出脉宽1ms 500HZ时钟信号。

定时器2做16位自动重装20ms定时，中断，从P3.0高速输出脉宽20ms 25HZ时钟信号。同时中断里从P1.0取反输出25HZ时钟信号。

******************************************/


#include "stdio.h"
#include	"config.h"
#include	"GPIO.h"
#include	"oled.h"
#include	"adc.h"
#include	"timer.h"
void key_s(void);

extern unsigned int key;     
extern unsigned char ad0,ad1;
extern int s_flag;
extern int error_sflag;
unsigned char rtc[6]; 			//ds1302
static	int wendu_count;
float rd_temp();
unsigned char wendu[4]={2,2,3,4};						//ds18b20
unsigned char wen=0;
unsigned int wendu_tmp=0;
/********************* Timer0中断函数************************/
void timer0_int (void) interrupt TIMER0_VECTOR
{

}

/********************* Timer1中断函数************************/
void timer1_int (void) interrupt TIMER1_VECTOR
{
	char ss;
	ss++;
	key_s();
	if(key==4)s_flag=3;
	if(key==3)s_flag=2;
	if(key==2)s_flag=1;
//		m=ReadTemperature();//DHT11数据采集
				wendu_count++;
				if(wendu_count==50)
				{
					wendu_tmp=rd_temp();
					wendu[0]=wendu_tmp/1000;
					wendu[1]=(wendu_tmp/100%10);
					wendu[2]=wendu_tmp/10%10;
					wendu[3]=wendu_tmp%10;
					wen=wendu[0]*10+wendu[1];
					wendu_count=0;
				}
	if(ss==8)
	{

			ss=0;
			OLED_ShowString(0,0,"MODE");  
			OLED_ShowNum(33,0,s_flag,2,16);		//显示模式 
			OLED_ShowCHinese(48,0,0);OLED_ShowCHinese(64,0,1);OLED_ShowCHinese(80,0,2);//OLED_ShowCHinese(96,0,15);OLED_ShowCHinese(112,0,16);			//黄海阳
			OLED_ShowCHinese(0,2,3);OLED_ShowCHinese(16,2,4);			//温度
//			OLED_ShowCHinese(70,2,5);OLED_ShowCHinese(86,2,6);		//湿度
//			OLED_ShowString(0,2,"tmp:");  
			OLED_ShowNum(32,2,wendu[0],1,16);		//显示温度整数
			OLED_ShowNum(40,2,wendu[1],1,16);
			OLED_ShowString(48,2,"."); 
			OLED_ShowNum(50,2,wendu[2],1,16);		//显示温度小数
			OLED_ShowNum(58,2,wendu[3],1,16);
//			OLED_ShowNum(105,2,humi,2,16);		//显示湿度
		 
		
			OLED_ShowCHinese(0,4,7);OLED_ShowCHinese(16,4,8);OLED_ShowCHinese(32,4,9);
			OLED_ShowNum(48,4,ad0,3,16);			//显示模拟量

			OLED_ShowCHinese(0,6,10);OLED_ShowCHinese(16,6,11);OLED_ShowCHinese(32,6,12);
			if(error_sflag==1)
			OLED_ShowCHinese(48,6,13);
			else OLED_ShowCHinese(48,6,14);
	}
}

/********************* Timer2中断函数************************/
void timer2_int (void) interrupt TIMER2_VECTOR
{
	unsigned char s;
	s++;
	if(s==200)
	{
		ad0=Get_ADC10bitResult(7);
		ad1=ad0* 5 / 1024 *100;
	}
}


//========================================================================
// 函数: u8	Timer_Inilize(u8 TIM, TIM_InitTypeDef *TIMx)
// 描述: 定时器初始化程序.
// 参数: TIMx: 结构参数,请参考timer.h里的定义.
// 返回: 成功返回0, 空操作返回1,错误返回2.
// 版本: V1.0, 2012-10-22
//========================================================================
u8	Timer_Inilize(u8 TIM, TIM_InitTypeDef *TIMx)
{
	if(TIM > Timer2)	return 1;	//空操作

	if(TIM == Timer0)
	{
		TR0 = 0;		//停止计数
		if(TIMx->TIM_Interrupt == ENABLE)		ET0 = 1;	//允许中断
		else									ET0 = 0;	//禁止中断
		if(TIMx->TIM_Polity == PolityHigh)		PT0 = 1;	//高优先级中断
		else									PT0 = 0;	//低优先级中断
		if(TIMx->TIM_Mode >  TIM_16BitAutoReloadNoMask)	return 2;	//错误
		TMOD = (TMOD & ~0x03) | TIMx->TIM_Mode;	//工作模式,0: 16位自动重装, 1: 16位定时/计数, 2: 8位自动重装, 3: 16位自动重装, 不可屏蔽中断
		if(TIMx->TIM_ClkSource == TIM_CLOCK_12T)	AUXR &= ~0x80;	//12T
		if(TIMx->TIM_ClkSource == TIM_CLOCK_1T)		AUXR |=  0x80;	//1T
		if(TIMx->TIM_ClkSource == TIM_CLOCK_Ext)	TMOD |=  0x04;	//对外计数或分频
		else										TMOD &= ~0x04;	//定时
		if(TIMx->TIM_ClkOut == ENABLE)	INT_CLKO |=  0x01;	//输出时钟
		else							INT_CLKO &= ~0x01;	//不输出时钟
		
		TH0 = (u8)(TIMx->TIM_Value >> 8);
		TL0 = (u8)TIMx->TIM_Value;
		if(TIMx->TIM_Run == ENABLE)	TR0 = 1;	//开始运行
		return	0;		//成功
	}

	if(TIM == Timer1)
	{
		TR1 = 0;		//停止计数
		if(TIMx->TIM_Interrupt == ENABLE)		ET1 = 1;	//允许中断
		else									ET1 = 0;	//禁止中断
		if(TIMx->TIM_Polity == PolityHigh)		PT1 = 1;	//高优先级中断
		else									PT1 = 0;	//低优先级中断
		if(TIMx->TIM_Mode >= TIM_16BitAutoReloadNoMask)	return 2;	//错误
		TMOD = (TMOD & ~0x30) | TIMx->TIM_Mode;	//工作模式,0: 16位自动重装, 1: 16位定时/计数, 2: 8位自动重装
		if(TIMx->TIM_ClkSource == TIM_CLOCK_12T)	AUXR &= ~0x40;	//12T
		if(TIMx->TIM_ClkSource == TIM_CLOCK_1T)		AUXR |=  0x40;	//1T
		if(TIMx->TIM_ClkSource == TIM_CLOCK_Ext)	TMOD |=  0x40;	//对外计数或分频
		else										TMOD &= ~0x40;	//定时
		if(TIMx->TIM_ClkOut == ENABLE)	INT_CLKO |=  0x02;	//输出时钟
		else							INT_CLKO &= ~0x02;	//不输出时钟
		
		TH1 = (u8)(TIMx->TIM_Value >> 8);
		TL1 = (u8)TIMx->TIM_Value;
		if(TIMx->TIM_Run == ENABLE)	TR1 = 1;	//开始运行
		return	0;		//成功
	}

	if(TIM == Timer2)		//Timer2,固定为16位自动重装, 中断无优先级
	{
		AUXR &= ~(1<<4);	//停止计数
		if(TIMx->TIM_Interrupt == ENABLE)			IE2  |=  (1<<2);	//允许中断
		else										IE2  &= ~(1<<2);	//禁止中断
		if(TIMx->TIM_ClkSource >  TIM_CLOCK_Ext)	return 2;
		if(TIMx->TIM_ClkSource == TIM_CLOCK_12T)	AUXR &= ~(1<<2);	//12T
		if(TIMx->TIM_ClkSource == TIM_CLOCK_1T)		AUXR |=  (1<<2);	//1T
		if(TIMx->TIM_ClkSource == TIM_CLOCK_Ext)	AUXR |=  (1<<3);	//对外计数或分频
		else										AUXR &= ~(1<<3);	//定时
		if(TIMx->TIM_ClkOut == ENABLE)	INT_CLKO |=  0x04;	//输出时钟
		else							INT_CLKO &= ~0x04;	//不输出时钟

		TH2 = (u8)(TIMx->TIM_Value >> 8);
		TL2 = (u8)TIMx->TIM_Value;
		if(TIMx->TIM_Run == ENABLE)	AUXR |=  (1<<4);	//开始运行
		return	0;		//成功
	}
	return 2;	//错误
}
