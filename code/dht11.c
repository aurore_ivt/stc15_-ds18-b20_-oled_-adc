#include <reg52.h>                                   // 头文件包含
#include <intrins.h>

#define uchar unsigned char        // 以后unsigned char就可以用unsigned char代替
#define uint  unsigned int        // 以后unsigned int 就可以用uint 代替

sbit DHT11_P   = P1^5;                 // 温湿度传感器DHT11数据接入

unsigned char temp;                                        // 保存温度
unsigned char humi;   
// 保存湿度

/*********************************************************/
// 毫秒级的延时函数，time是要延时的毫秒数
/*********************************************************/



/*********************************************************/
// 10us级延时程序
/*********************************************************/
void Delay20ms()		//@11.0592MHz
{
	unsigned char i, j, k;

	_nop_();
	_nop_();
	i = 1;
	j = 216;
	k = 35;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}
void Delay30us()		//@11.0592MHz
{
	unsigned char i;

	_nop_();
	_nop_();
	i = 80;
	while (--i);
}

/*********************************************************/
// 读取DHT11单总线上的一个字节
/*********************************************************/
unsigned char DhtReadByte(void)
{   
				bit bit_i;
				unsigned char j;
				unsigned char dat=0;

				for(j=0;j<8;j++)   
				{
								while(!DHT11_P);        // 等待低电平结束      
								Delay30us();                        // 延时
								if(DHT11_P==1)                // 判断数据线是高电平还是低电平
								{
												bit_i=1;
												while(DHT11_P);
								}
								else
								{
												bit_i=0;
								}
								dat<<=1;                                   // 将该位移位保存到dat变量中
								dat|=bit_i;   
				}
				return(dat);  
}



/*********************************************************/
// 读取DHT11的一帧数据，湿高、湿低(0)、温高、温低(0)、校验码
/*********************************************************/
void ReadDhtData()
{            
				unsigned char HumiHig;                // 湿度高检测值
				unsigned char HumiLow;                // 湿度低检测值
				unsigned char TemHig;                        // 温度高检测值
				unsigned char TemLow;                        // 温度低检测值
				unsigned char check;                        // 校验字节
		 
				DHT11_P=0;                                // 主机拉低
				Delay20ms();
				//DelayMs(20);                        // 保持20毫秒
				DHT11_P=1;                                // DATA总线由上拉电阻拉高

				Delay30us();                         // 延时等待30us

				while(!DHT11_P);        // 等待DHT的低电平结束
				while(DHT11_P);                // 等待DHT的高电平结束

				//进入数据接收状态
				HumiHig = DhtReadByte();         // 湿度高8位
				HumiLow = DhtReadByte();         // 湿度低8为，总为0
				TemHig  = DhtReadByte();         // 温度高8位
				TemLow  = DhtReadByte();         // 温度低8为，总为0
				check   = DhtReadByte();        // 8位校验码，其值等于读出的四个字节相加之和的低8位

				DHT11_P=1;                                // 拉高总线

				if(check==HumiHig + HumiLow + TemHig + TemLow)                 // 如果收到的数据无误
				{
					temp=TemHig;                         // 将温度的检测结果赋值给全局变量temp
					humi=HumiHig;                        // 将湿度的检测结果赋值给全局变量humi
				}
}


